const jwt = require('jsonwebtoken')
const secret = "pizza"

const employeeAuth = (req, res, next) => {
    //on récupère notre token dans le header de la requète HTTP
    const token = req.headers['x-access-token']
    console.log("ici on vérifie le token", token)
    //si il ne trouve pas de token
    if(token === undefined){
        res.json({status: 404, msg: "error2, token not found."})
    } else {
        //sinon il a trouvé un token, utilisation de la fonction de vérification du token
        jwt.verify(token, secret, (err, decoded)=>{
            if(err){
                res.json({status: 401, msg: "error: your token is invalid."})
            } else {
                if (decoded.role === "admin" || decoded.role === "employee") {
                    req.id = decoded.id
                    req.email = decoded.email
                }
                //le token est vérifié et valide
                //on rajoute une propriété id dans req, qui récupère l'id du token décrypté
                //on sort de la fonction, on autorise l'accés à la callback de la route back demandée
                console.log("token vérifié on passe à la fonction")
                next()
            }
        })
    }
}

module.exports = employeeAuth