-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 08 jan. 2024 à 22:19
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pizza`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Les classiques', 'Pour les vrais fans de pizza'),
(3, 'Les boissons', 'Pour se rafraichir');

-- --------------------------------------------------------

--
-- Structure de la table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `customersID` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lastName` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `addressLineOne` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `addressLineTwo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `postalCode` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `key_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `latitude` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `longitude` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `connexionTimesTamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `validate` varchar(5) CHARACTER SET swe7 COLLATE swe7_swedish_ci NOT NULL,
  PRIMARY KEY (`customersID`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `customers`
--

INSERT INTO `customers` (`customersID`, `firstName`, `lastName`, `phone`, `email`, `addressLineOne`, `addressLineTwo`, `postalCode`, `city`, `password`, `key_id`, `latitude`, `longitude`, `connexionTimesTamp`, `validate`) VALUES
(22, 'Michelle', 'Christophe', '0612345789', 'michelle.christophe@gmail.com', '1 rue du faubourg Saint antoine', '', '75012', 'Paris', '$2a$10$oc7GgbxHFMR2HLBkv2WTeOaV93K9pitlb46466gZZwCNVJtbnUP/K', 'CQnzIbo1WuuDb5Niw96Z87bbLpGsbG', '', '', '2023-10-11 12:46:05', 'no'),
(26, 'Samuel', 'Planck', '0203040506', 'Samuel3Planck@gmail.com', '2 rue du rendez vous', 'u', '75012', 'Paris', '$2a$10$D32hqe7iYV94YodoxIYrn.YnsKw6maLKv15TdbbVe0pQhm4mxQObm', 'czgQzOShmhaL81KaGKIIo80rdwJQCY', '', '', '2023-10-11 12:45:06', 'no'),
(46, 'Jean', 'Pierre', '0102030405', 'jp@gmail.com', '80 boulevard soult', '', '75012', 'paris', '$2a$10$xIyiU/vwJv5wS8hFHxDeZunl1fwrpPd1Kybc.r4lwDJSL1.qeHkT2', 'twXx8cDhSN13GqmseNFKPCIU3KZpoy', '', '', '2023-10-11 12:43:52', 'no'),
(49, 'remi', 'Abdallah', '0666149044', 'remiabdallah3@gmail.com', '22 rue de la voute', '', '75012', 'Paris', '$2a$10$pqHx.XC9DMG6JCqqY4U30OMYcv5KgYXpiXOWFT0DK0BjmQvtqosCS', 'vF4YzMBYnLYnCYSz8rWmMBt5wHzbPi', '48.8456695', '2.4073877', '2024-01-08 20:58:17', 'yes'),
(50, 'a', 'z', '0102030405', 'remi.abdallah@free.fr', '22 rue de la voute', '', '75012', 'Paris', '$2a$10$3DhBLDH8C75yaTEoq4mz3uiHYddKpPzUcC628N3ggGr5yv4Ddqwsy', 'E8hw7B7qbHs19ENHkvI2ArcQ4sDM4P', '', '', '2023-10-25 08:43:52', 'no');

-- --------------------------------------------------------

--
-- Structure de la table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `employeeID` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lastName` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `addressLineOne` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `addressLineTwo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `postalCode` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `latitude` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `longitude` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `key_id` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `code` varchar(4) CHARACTER SET swe7 COLLATE swe7_swedish_ci NOT NULL,
  PRIMARY KEY (`employeeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `employees`
--

INSERT INTO `employees` (`employeeID`, `firstName`, `lastName`, `phone`, `email`, `role`, `addressLineOne`, `addressLineTwo`, `postalCode`, `city`, `password`, `latitude`, `longitude`, `key_id`, `code`) VALUES
(1, 'remi', 'Abdallah', '0102030405', 'remiabdallah4@gmail.com', 'admin', '22 rue de la voute', '', '75012', 'Paris', '$2a$10$ZPKUlU1Hp13purr/MjiNDOhbE4IDQXBxYjCDYyZqivYwF9lzxM3Bu', NULL, NULL, 'ca3CTjgNfaCX7U2AZNE0RECG3wfi5F', '6538');

-- --------------------------------------------------------

--
-- Structure de la table `orderdetails`
--

DROP TABLE IF EXISTS `orderdetails`;
CREATE TABLE IF NOT EXISTS `orderdetails` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `orderId` int NOT NULL,
  `productId` int DEFAULT NULL,
  `quantity` int NOT NULL,
  `priceEach` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `productId` (`productId`),
  KEY `orderId` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `orderdetails`
--

INSERT INTO `orderdetails` (`ID`, `orderId`, `productId`, `quantity`, `priceEach`) VALUES
(175, 68, 6, 3, 15),
(176, 68, 9, 1, 14),
(177, 69, 6, 3, 15),
(178, 69, 9, 1, 14),
(179, 70, 6, 3, 15),
(180, 70, 9, 1, 14),
(181, 71, 6, 3, 15),
(182, 71, 9, 1, 14),
(183, 72, 6, 3, 15),
(184, 72, 9, 1, 14),
(185, 73, 6, 3, 15),
(186, 73, 9, 1, 14),
(187, 74, 6, 3, 15),
(188, 74, 9, 1, 14),
(189, 75, 6, 3, 15),
(190, 75, 9, 1, 14),
(191, 76, 6, 3, 15),
(192, 76, 9, 1, 14),
(193, 77, 6, 3, 15),
(194, 77, 9, 1, 14),
(195, 78, 6, 3, 15),
(196, 78, 9, 1, 14),
(197, 79, 6, 3, 15),
(198, 79, 9, 1, 14),
(199, 82, 6, 3, 15),
(200, 82, 9, 1, 14),
(201, 83, 6, 3, 15),
(202, 83, 1, 2, 12),
(203, 84, 6, 3, 15),
(204, 84, 1, 2, 12),
(205, 85, 6, 3, 15),
(206, 85, 1, 2, 12),
(207, 86, 6, 3, 15),
(208, 86, 1, 2, 12),
(209, 87, 6, 3, 15),
(210, 87, 1, 2, 12),
(211, 88, 6, 3, 15),
(212, 88, 1, 2, 12),
(213, 89, 6, 3, 15),
(214, 89, 1, 2, 12),
(215, 90, 6, 3, 15),
(216, 90, 1, 2, 12),
(217, 91, 6, 3, 15),
(218, 91, 1, 2, 12),
(219, 91, 9, 1, 14),
(220, 92, 9, 1, 14),
(221, 92, 6, 1, 15),
(222, 92, 1, 1, 12),
(223, 93, 9, 1, 14),
(224, 93, 6, 1, 15),
(225, 93, 1, 1, 12),
(226, 94, 6, 2, 15),
(227, 96, 9, 1, 14),
(228, 96, 6, 1, 15),
(229, 97, 9, 1, 14),
(230, 97, 6, 1, 15),
(231, 98, 9, 1, 14),
(232, 98, 6, 1, 15),
(233, 99, 9, 1, 14),
(234, 99, 6, 1, 15),
(235, 100, 1, 1, 12);

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `ordersID` int NOT NULL AUTO_INCREMENT,
  `customersID` int DEFAULT NULL,
  `orderDate` date NOT NULL,
  `shippedDate` date DEFAULT NULL,
  `status` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `comments` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `phone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `addressLineOne` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `addressLineTwo` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `postalCode` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `city` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `amount` int NOT NULL,
  PRIMARY KEY (`ordersID`),
  KEY `customersID` (`customersID`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `orders`
--

INSERT INTO `orders` (`ordersID`, `customersID`, `orderDate`, `shippedDate`, `status`, `comments`, `phone`, `addressLineOne`, `addressLineTwo`, `postalCode`, `city`, `latitude`, `longitude`, `amount`) VALUES
(68, NULL, '2023-11-14', NULL, 'canceled', '', NULL, '', '', '', '', NULL, NULL, 59),
(69, 49, '2023-11-15', NULL, 'canceled', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(70, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(71, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(72, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(73, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(74, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(75, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(76, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(77, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(78, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(79, 49, '2023-11-15', NULL, 'waiting for payment', '', '0666149044', '', '', '', '', NULL, NULL, 59),
(82, NULL, '2023-11-22', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 59),
(83, NULL, '2023-12-01', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 69),
(84, NULL, '2023-12-04', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 69),
(85, NULL, '2023-12-04', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 69),
(86, NULL, '2023-12-04', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 69),
(87, NULL, '2023-12-05', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 69),
(88, NULL, '2023-12-05', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 69),
(89, NULL, '2023-12-05', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 69),
(90, NULL, '2023-12-06', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 69),
(91, NULL, '2023-12-07', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 83),
(92, NULL, '2023-12-07', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 41),
(93, NULL, '2023-12-07', NULL, 'waiting for payment', '', '', '', '', '', '', NULL, NULL, 41),
(94, NULL, '2023-12-28', NULL, 'paid', '', NULL, '', '', '', '', NULL, NULL, 30),
(95, 49, '2024-01-08', NULL, 'waiting for payment', 'aaa', '0666149044', '22 rue de la voute', '', '75012', 'Paris', 48.8457, 2.40739, 29),
(96, 49, '2024-01-08', NULL, 'paid', 'aaa', '0666149044', '22 rue de la voute', '', '75012', 'Paris', 48.8457, 2.40739, 29),
(97, 49, '2024-01-08', NULL, 'paid', '', '0666149044', '22 rue de la voute', '', '75012', 'Paris', 48.8457, 2.40739, 29),
(98, 49, '2024-01-08', NULL, 'paid', '', '0666149044', '22 rue de la voute', '', '75012', 'Paris', 48.8457, 2.40739, 29),
(99, 49, '2024-01-08', NULL, 'paid', '', '0666149044', '22 rue de la voute', '', '75012', 'Paris', 48.8457, 2.40739, 29),
(100, 49, '2024-01-08', NULL, 'paid', '', '0666149044', '22 rue de la voute', '', '75012', 'Paris', 48.8457, 2.40739, 12);

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ingredients` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alt` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `price` int NOT NULL,
  `categoryId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryId` (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `content`, `ingredients`, `url`, `alt`, `price`, `categoryId`) VALUES
(1, 'Margherita', 'la pizza classique', 'rien', 'pizza.jpg', 'pizza margherita', 12, 1),
(4, '4 fromages', 'La super star des pizzas', 'Mozarella, bleu, buches de chèvre, parmesan', 'pizza.jpg', 'pizza 4 fromages', 14, NULL),
(5, 'Saumon', 'Tout ce que nous voulons c\'est du poisson fort bien gouteux', 'saumon, citron, crème fraiche, mozarella', 'pizza.jpg', 'pizza au saumon', 15, NULL),
(6, 'Reggina', 'La Reine Elizabe... euh non', 'Champignons, Jambon, mozarella', 'pizza.jpg', 'Pizza Reggina', 15, 1),
(9, 'Merguez', 'Saveurs d\'Orient', 'merguez, poivrons, oignons, oeufs, mozarella', 'pizza.jpg', '', 14, 1),
(10, 'Potager', 'Parce-que c\'est bon sans viande aussi', 'Artichauds, poivrons, oignons, mozarella, champignons', 'pizza.jpg', '', 14, 1),
(11, 'Bolognaise', 'Fan de viande hachée', 'Boeuf haché, mozarella, oignons', 'pizza.jpg', '', 14, 1),
(12, 'Ice Tea', 'thé glacé parfumé', 'thé, sucre, aromes', 'coca.jpg', 'Ice Tea', 3, 3),
(14, 'May Tea', 'Thé glacé parfulé', 'thé, sucre, arome', 'coca.jpg', 'May Tea', 4, 3),
(15, 'Fuze Tea', 'Thé glacé parfumé', 'thé, sucre', 'coca.jpg', 'Fuze Tea', 3, 3),
(16, 'Thé noir', 'un thé noir classique', 'thé', 'coca.jpg', 'thé', 5, 3);

-- --------------------------------------------------------

--
-- Structure de la table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
CREATE TABLE IF NOT EXISTS `restaurant` (
  `name` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `addressLineOne` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `addressLineTwo` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `postalCode` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `phone` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `startLunchMonday` time DEFAULT NULL,
  `endLunchMonday` time DEFAULT NULL,
  `startDinnerMonday` time DEFAULT NULL,
  `endDinnerMonday` time DEFAULT NULL,
  `startLunchTuesday` time DEFAULT NULL,
  `endLunchTuesday` time DEFAULT NULL,
  `startDinnerTuesday` time DEFAULT NULL,
  `endDinnerTuesday` time DEFAULT NULL,
  `startLunchWednesday` time DEFAULT NULL,
  `endLunchWednesday` time DEFAULT NULL,
  `startDinnerWednesday` time DEFAULT NULL,
  `endDinnerWednesday` time DEFAULT NULL,
  `startLunchThursday` time DEFAULT NULL,
  `endLunchThursday` time DEFAULT NULL,
  `startDinnerThursday` time DEFAULT NULL,
  `endDinnerThursday` time DEFAULT NULL,
  `startLunchFriday` time DEFAULT NULL,
  `endLunchFriday` time DEFAULT NULL,
  `startDinnerFriday` time DEFAULT NULL,
  `endDinnerFriday` time DEFAULT NULL,
  `startLunchSaturday` time DEFAULT NULL,
  `endLunchSaturday` time DEFAULT NULL,
  `startDinnerSaturday` time DEFAULT NULL,
  `endDinnerSaturday` time DEFAULT NULL,
  `startLunchSunday` time DEFAULT NULL,
  `endLunchSunday` time DEFAULT NULL,
  `startDinnerSunday` time DEFAULT NULL,
  `endDinnerSunday` time DEFAULT NULL,
  `message` text CHARACTER SET latin1 COLLATE latin1_swedish_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `restaurant`
--

INSERT INTO `restaurant` (`name`, `content`, `addressLineOne`, `addressLineTwo`, `postalCode`, `city`, `latitude`, `longitude`, `phone`, `email`, `startLunchMonday`, `endLunchMonday`, `startDinnerMonday`, `endDinnerMonday`, `startLunchTuesday`, `endLunchTuesday`, `startDinnerTuesday`, `endDinnerTuesday`, `startLunchWednesday`, `endLunchWednesday`, `startDinnerWednesday`, `endDinnerWednesday`, `startLunchThursday`, `endLunchThursday`, `startDinnerThursday`, `endDinnerThursday`, `startLunchFriday`, `endLunchFriday`, `startDinnerFriday`, `endDinnerFriday`, `startLunchSaturday`, `endLunchSaturday`, `startDinnerSaturday`, `endDinnerSaturday`, `startLunchSunday`, `endLunchSunday`, `startDinnerSunday`, `endDinnerSunday`, `message`) VALUES
('PizzaPlace Bastille', 'Le meilleur resto de pizza de Paris', '1 rue du Faubourg Saint-Antoine', '', '75012', 'Paris', 48.8533, 2.37052, '0102030405', 'PizzaPLace?contact.org', '12:00:00', '14:00:00', '18:00:00', '22:00:00', '12:00:00', '14:00:00', '18:00:00', '22:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12:00:00', '14:00:00', '18:00:00', '22:00:00', '12:00:00', '14:00:00', '18:00:00', '22:00:00', '12:00:00', '14:00:00', '18:00:00', '22:00:00', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD CONSTRAINT `orderdetails_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `products` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `orderdetails_ibfk_3` FOREIGN KEY (`orderId`) REFERENCES `orders` (`ordersID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customersID`) REFERENCES `customers` (`customersID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
