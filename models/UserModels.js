const db = require("../db")
const bcrypt = require('bcryptjs');
const saltRounds = 10;
let randomId = require('random-id');
let len = 30;
let pattern = 'aA0'

//Code conservant les requêtes à la base de données
class UserModel {

	//requête sauvegardant un client
    static async saveOneCustomer(req, callback) {
        let sql = "INSERT INTO customers (firstName, lastName, phone, email, addressLineOne, addressLineTwo, postalCode, city, password, key_id, latitude, longitude, connexionTimestamp, validate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 'no')"
        let key_id = randomId(len, pattern)
        return bcrypt.hash(req.body.password, saltRounds)
        .then((hash) => {
            db.query(sql, [req.body.firstName, req.body.lastName, req.body.phone, req.body.email, req.body.addressLineOne, req.body.addressLineTwo, req.body.postalCode, req.body.city, hash,  key_id, req.body.latitude, req.body.longitude], (err, res) => {
				if (err) {
					callback(err, null)
				} else {
					callback(null, res)
				}
			})
        })
        .catch((err) => {
            callback(err, null)
        })
    }

	//requête validant le compte d'un client
    static async updateValidateCustomer(key_id, callback){
	    let sql = "UPDATE customers SET validate = 'yes' WHERE key_id = ?"
	    db.query(sql, [key_id], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}

	//requête modifiant la key_id d'un client
    static async updateKeyIdCustomer(email, callback){
		let key_id = randomId(len, pattern)
	    let sql = "UPDATE customers SET key_id = ? WHERE email = ?"
		db.query(sql, [key_id, email], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}

	//requête modifiant le mot de passe d'un client
    static async updatePasswordCustomer(newPassword, key_id, callback){
	    return bcrypt.hash(newPassword, saltRounds)
		.then((hash) => {
			let sql = "UPDATE customers SET password = ? WHERE key_id = ?"
			db.query(sql, [hash, key_id], (err, res) => {
				if (err) {
					callback(err, null)
				} else {
					callback(null, res)
				}
			})
		})
		.catch((err) => {
			callback(err, null)
		})
	}

	//requête récupérant un client par son email
    static async getCustomerByMail(email, callback){
	    let sql = "SELECT * FROM customers WHERE email = ?"
		db.query(sql, [email], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}

	//requête récupérant un client par sa key_id
    static async getCustomerByKeyId(key_id, callback){
	    let sql = "SELECT * FROM customers WHERE key_id = ?"
		db.query(sql, [key_id], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}

	//requête récupérant un client par son id
	static async getCustomerById(id, callback) {
		let sql = "SELECT * FROM customers WHERE id = ?"
		db.query(sql, [id], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}

	//requête modifiant les informations d'un client
    static async updateCustomer(req, key_id, newEmail, callback){
		if(newEmail) {
			let sql = "UPDATE customers SET firstName = ?, lastName = ?, phone = ?, email = ?, addressLineOne = ?, addressLineTwo = ?, postalCode = ?, city = ?, latitude = ?, longitude = ?, connexionTimestamp = NOW(), validate = 'no' WHERE key_id = ?"
			db.query(sql, [req.body.firstName, req.body.lastName, req.body.phone, req.body.email, req.body.addressLineOne, req.body.addressLineTwo, req.body.postalCode, req.body.city, req.body.latitude, req.body.longitude, key_id], (err, res) => {
				if (err) {
					callback(err, null)
				} else {
					callback(null, res)
				}
			})
		} else {
			let sql = "UPDATE customers SET firstName = ?, lastName = ?, phone = ?, addressLineOne = ?, addressLineTwo = ?, postalCode = ?, city = ?, latitude = ?, longitude = ?, connexionTimestamp = NOW() WHERE key_id = ?"
			db.query(sql, [req.body.firstName, req.body.lastName, req.body.phone, req.body.addressLineOne, req.body.addressLineTwo, req.body.postalCode, req.body.city, req.body.latitude, req.body.longitude, key_id], (err, res) => {
				if (err) {
					callback(err, null)
				} else {
					callback(null, res)
				}
			})
		}
    }

	//requête sup primant un client
    static async deleteCustomer(key_id, callback) {
        let sql = "DELETE FROM customers WHERE key_id = ?"
        db.query(sql, [key_id], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
    }

	//requête récupérant tous les clients
	static async getAllCustomers(callback) {
		let sql = "SELECT * FROM customers"
		db.query(sql, (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}

	//requête invalidant le compte d'un client
	static async unvalidate(key_id, callback) {
		let sql = "UPDATE customers PUT validate = no WHERE key_id = ?"
		db.query(sql, [key_id], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}

	///////////////////////////////////////
	///////////////////////////////////////
	///////////////////////////////////////

	//requête sauvegardant un employé
	static async saveOneEmployee(req, callback) {
		let sql = "INSERT INTO employees (firstName, lastName, phone, email, role, addressLineOne, addressLineTwo, postalCode, city, password, key_id, code) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
		let key_id = randomId(len, pattern)
		return bcrypt.hash(req.body.password, saltRounds)
		.then((hash) => {
			db.query(sql, [req.body.firstName, req.body.lastName, req.body.phone, req.body.email, req.body.role, req.body.addressLineOne, req.body.addressLineTwo, req.body.postalCode, req.body.city, hash, key_id, req.body.code], (err, res) => {
				if (err) {
					callback(err, null)
				} else {
					callback(null, res)
				}
			})
		})
		.catch((err) => {
			callback(err, null)
		})
	}
	
	//requête modifiant les informations d'un employé
	static async editEmployee (req, id, callback) {
		let sql = "UPDATE employees SET firstName = ?, lastName = ?, phone = ?, email = ?, role = ?, addressLineOne = ?, addressLineTwo = ?, postalCode = ?, city = ?, password = ? WHERE employeeID = ?"
		return bcrypt.hash(req.body.password, saltRounds)
		.then((hash) => {
			db.query(sql, [req.body.firstName, req.body.lastName, req.body.phone, req.body.email, req.body.role, req.body.addressLineOne, req.body.addressLineTwo, req.body.postalCode, req.body.city, hash, id], (err, res) => {
				if (err) {
					callback(err, null)
				} else {
					callback(null, res)
				}
			})
		})
		.catch((err) => {
			callback(err, null)
		})
	}

	//requête supprimant un employé
	static async deleteEmployee (id, callback) {
		let sql = "DELETE FROM employees WHERE employeeID = ?"
		db.query(sql, [id], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
		
	}

	//requête récupérant un employé par son email
	static async getEmployeeByMail (email, callback) {
		let sql = "SELECT * FROM employees WHERE email = ?"
		db.query(sql, [email], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
		
	}

	//requête récupérant un employé par sa key_id
    static async getEmployeeByKeyId(key_id, callback){
	    let sql = "SELECT * FROM employees WHERE key_id = ?"
		db.query(sql, [key_id], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
		
	}

	static async getEmployeeById(id, callback) {
		let sql = "SELECT * FROM employees WHERE employeeID = ?"
		db.query(sql, [id], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
		
	}

	//requête récupérant tous les employés
	static async getAllEmployees(callback) {
		let sql = "SELECT * FROM employees"
		db.query(sql, (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}

	//requête récupérant un employé par son code
	static async getEmployeeByCode(code, callback) {
		let sql = "SELECT * FROM employees WHERE code = ?"
		db.query(sql, [code], (err, res) => {
			if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
		})
	}
}
module.exports = UserModel;