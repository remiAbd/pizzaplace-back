const db = require("../db")

//Code conservant les requêtes à la base de données
class OrderModel {

    //Requête récupérant toutes les commandes
    static async getAllOrders(callback) {
        let sql = "SELECT * FROM orders"
        db.query(sql, (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête récupérant une commande par son id
    static async getOneOrder(id, callback) {
        let sql = "SELECT * FROM orders WHERE ordersID = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête récupérant toutes les commandes en attente
    static async getAllCurrentOrders(callback) {
        let sql = "SELECT * FROM `orders` WHERE STATUS != 'shipped'"
        db.query(sql, (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }


    //Requête récupérant toutes les commandes livrées
    static async getAllPastOrders(callback) {
        let sql = "SELECT * FROM orders WHERE status = 'shipped'"
        db.query(sql, (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }

    //Requête récupérant toutes les commandes d'un client
    static async getAllOrdersFrom(id, callback) {
        let sql = "SELECT * FROM orders WHERE customersID = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête permettant de sauvegarder une commande
    static async saveOneOrder(req, callback) {
        let sql = "INSERT INTO orders (customersId, orderDate, shippedDate, status, comments, phone, addressLineOne, addressLineTwo, postalCode, city, latitude, longitude, amount) VALUES (?, NOW(), NULL, 'waiting for payment', ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        db.query(sql, [req.body.customersId, req.body.comments, req.body.phone, req.body.addressLineOne, req.body.addressLineTwo, req.body.postalCode, req.body.city, req.body.latitude, req.body.longitude, req.body.amount], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête modifiant le status d'une commande
    static async updateOrderStatus(status, id, callback) {
        let sql = "UPDATE orders SET status = ? WHERE ordersID = ?"
        db.query(sql, [status, id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête supprimant une commande par son id 
    static async deleteOneOrder(id, callback) {
        let sql = "DELETE FROM orders WHERE ordersID = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête modifiant le status d'une commande en la désignant comme envoyée
    static async setShipped(id, callback) {
        let sql = "UPDATE orders SET shippedDate = NOW(), status = 'sent' WHERE ordersID = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête modifiant le status d'une commande comme payée
    static async setPaid(id, callback) {
        let sql = "UPDATE orders SET status = 'paid' WHERE ordersID = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }

    /////////////////////////////////////////
    /////////////////////////////////////////
    /////////////////////////////////////////
    //Requête sauvegardant les détails d'une commande
    static async saveOrderDetail(orderId, basket, callback) {
        let sql = "INSERT INTO orderdetails (orderId, productId, quantity, priceEach) VALUES (?, ?, ?, ?)"
        db.query(sql, [orderId, basket.product.id, basket.quantity, basket.product.price], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête supprimant les détails d'une commande
    static async deleteOrderDetail(id, callback) {
        let sql = "DELETE FROM orderdetails WHERE id = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête modifiant le détail d'une commande
    static async editOrderDetail(id, callback) {
        let sql = "SET orderdetails orderId = ?, productId = ?, quantity = ?, priceEach = ? WHERE id = ?"
        db.query(sql, [req.body.orderId, req.body.productId, req.body.quantity, req.body.priceEach, id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête récupérant les détails d'une commande
    static async getOrderDetails(orderId, callback) {
        let sql = "SELECT * FROM orderdetails WHERE orderId = ?"
        db.query(sql, [orderId], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //Requête récupérant tous les détails d'une commande ainsi que les informations des produits
    static async getFullDetails(id, callback) {
        let sql = "SELECT orderdetails.ID, quantity, name, content, url, alt, price FROM orderdetails INNER JOIN products ON orderdetails.productId = products.id WHERE orderId = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

}

module.exports = OrderModel;