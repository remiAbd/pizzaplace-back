const db = require("../db")

//Code conservant les requêtes à la base de données
class CategoryModel {

    //requête récupérant toutes les catégories
    static async getAllCategories(callback) {
        let sql = "SELECT * FROM categories"
        db.query(sql, (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //requête récupérant une catégorie par son id
    static async getOneCategory(id, callback) {
        let sql = "SELECT * FROM categories WHERE id = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //requête sauvegardant une catégorie
    static async saveOneCategory(req, callback) {
        let sql = "INSERT INTO categories (name, description) VALUES (?, ?)"
        db.query(sql, [req.body.name, req.body.description], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //requête modifiant une catégorie
    static async editOneCategory(req, id, callback) {
        let sql = "UPDATE categories SET name = ?, description = ? WHERE id = ?"
        db.query(sql, [req.body.name, req.body.description, id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }

    //requête supprimant une catégorie
    static async deleteOneCategory(id, callback) {
        let sql = "DELETE FROM categories WHERE id = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }
}
module.exports = CategoryModel;