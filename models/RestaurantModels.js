const db = require("../db")

//Code conservant les requêtes à la base de données
class restaurantModel {

    //requête récupérant les informations des restaurant
    static async getRestaurant(callback) {
        let sql = "SELECT * from restaurant"
        db.query(sql, (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }

    //requête sauvegardant les informations d'un restaurant
    static async saveRestaurant(req, callback) {
        let sql = "INSERT INTO restaurant (`name`, `content`, `addressLineOne`, `addressLineTwo`, `postalCode`, `city`, `latitude`, `longitude`, `phone`, `email`, `startLunchMonday`, `endLunchMonday`, `startDinnerMonday`, `endDinnerMonday`, `startLunchTuesday`, `endLunchTuesday`, `startDinnerTuesday`, `endDinnerTuesday`, `startLunchWednesday`, `endLunchWednesday`, `startDinnerWednesday`, `endDinnerWednesday`, `startLunchThursday`, `endLunchThursday`, `startDinnerThursday`, `endDinnerThursday`, `startLunchFriday`, `endLunchFriday`, `startDinnerFriday`, `endDinnerFriday`, `startLunchSaturday`, `endLunchSaturday`, `startDinnerSaturday`, `endDinnerSaturday`, `startLunchSunday`, `endLunchSunday`, `startDinnerSunday`, `endDinnerSunday`, `message`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        db.query(sql, [req.body.name, req.body.content, req.body.addressLineOne, req.body.addressLineTwo, req.body.postalCode, req.body.city, req.body.latitude, req.body.longitude, req.body.phone, req.body.email, req.body.startLunchMonday, req.body.endLunchMonday, req.body.startDinnerMonday, req.body.endDinnerMonday, req.body.startLunchTuesday, req.body.endLunchTuesday, req.body.startDinnerTuesday, req.body.endDinnerTuesday, req.body.startLunchWednesday, req.body.endLunchWednesday, req.body.startDinnerWednesday, req.body.endDinnerWednesday, req.body.startLunchThursday, req.body.endLunchThursday, req.body.startDinnerThursday, req.body.endDinnerThursday, req.body.startLunchFriday, req.body.endLunchFriday, req.body.startDinnerFriday, req.body.endDinnerFriday, req.body.startLunchSaturday, req.body.endLunchSaturday, req.body.startDinnerSaturday, req.body.endDinnerSaturday, req.body.startLunchSunday, req.body.endLunchSunday, req.body.startDinnerSunday, req.body.endDinnerSunday, req.body.message], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //requête modifiant les informations d'un restaurant
    static async updateRestaurant(id, req, callback) {
        let sql = "UPDATE restaurant SET `name` = ?, `content` = ?, `addressLineOne` = ?, `addressLineTwo` = ?, `postalCode` = ?, `city` = ?, `latitude` = ?, `longitude` = ?, `phone` = ?, `email` = ?, `startLunchMonday` = ?, `endLunchMonday` = ?, `startDinnerMonday` = ?, `endDinnerMonday` = ?, `startLunchTuesday` = ?, `endLunchTuesday` = ?, `startDinnerTuesday` = ?, `endDinnerTuesday` = ?, `startLunchWednesday` = ?, `endLunchWednesday` = ?, `startDinnerWednesday` = ?, `endDinnerWednesday` = ?, `startLunchThursday` = ?, `endLunchThursday` = ?, `startDinnerThursday` = ?, `endDinnerThursday` = ?, `startLunchFriday` = ?, `endLunchFriday` = ?, `startDinnerFriday` = ?, `endDinnerFriday` = ?, `startLunchSaturday` = ?, `endLunchSaturday` = ?, `startDinnerSaturday` = ?, `endDinnerSaturday` = ?, `startLunchSunday` = ?, `endLunchSunday` = ?, `startDinnerSunday` = ?, `endDinnerSunday` = ?, `message` WHERE id = ?"
        db.query(sql, [req.body.name, req.body.content, req.body.addressLineOne, req.body.addressLineTwo, req.body.postalCode, req.body.city, req.body.latitude, req.body.longitude, req.body.phone, req.body.email, req.body.startLunchMonday, req.body.endLunchMonday, req.body.startDinnerMonday, req.body.endDinnerMonday, req.body.startLunchTuesday, req.body.endLunchTuesday, req.body.startDinnerTuesday, req.body.endDinnerTuesday, req.body.startLunchWednesday, req.body.endLunchWednesday, req.body.startDinnerWednesday, req.body.endDinnerWednesday, req.body.startLunchThursday, req.body.endLunchThursday, req.body.startDinnerThursday, req.body.endDinnerThursday, req.body.startLunchFriday, req.body.endLunchFriday, req.body.startDinnerFriday, req.body.endDinnerFriday, req.body.startLunchSaturday, req.body.endLunchSaturday, req.body.startDinnerSaturday, req.body.endDinnerSaturday, req.body.startLunchSunday, req.body.endLunchSunday, req.body.startDinnerSunday, req.body.endDinnerSunday, req.body.message, id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
        
    }

    //requête supprimant les informations d'un restaurant
    static async deleteRestaurant(id, callback) {
        let sql = "DELETE restaurant WHERE id = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }
}
module.exports = restaurantModel;