const db = require("../db")
console.log("db dans models : ", db)
//Code conservant les requêtes à la base de données
class ProductModel {
    //requête sauvegardant un produit
    static async saveOneProduct(req, callback) {
        let sql = "INSERT INTO products (name, content, ingredients, url, alt, price, categoryId) VALUES (?, ?, ?, ?, ?, ?, ?)"
        db.query(sql, [req.body.name, req.body.content, req.body.ingredients, req.body.url, req.body.alt, req.body.price, req.body.categoryId], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }

    //requête modifiant un produit
    static async editOneProduct(req, id, callback) {
        let sql = "UPDATE products SET name = ?, content = ?, ingredients = ?, url = ?, alt = ?, price = ?, categoryId = ? WHERE id = ?"
        db.query(sql, [req.body.name, req.body.content, req.body.ingredients, req.body.url, req.body.alt, req.body.price, req.body.categoryId, id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }

    //requête supprimant un produit
    static async deleteOneProduct(id, callback) {
        let sql = "DELETE FROM products WHERE id = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }

    //requête récupérant un produit par son id
    static async getOneProduct(id, callback) {
        let sql = "SELECT * FROM products WHERE id = ?"
        db.query(sql, [id], (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }

    //requête récupérant tous les produits
    static async getAllProducts(callback) {
        let sql = "SELECT * FROM products"
        db.query(sql, (err, res) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, res)
            }
        })
    }
}
module.exports = ProductModel;