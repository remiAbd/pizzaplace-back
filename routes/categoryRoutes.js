const withAuth = require('../withAuth');
const adminAuth = require('../adminAuth');
const employeeAuth = require('../employeeAuth');
const fs = require('fs')

module.exports = (app, db, configSecret) => {
    const CategoryControllers = require("../controllers/categoryControllers")(db)

    //Route de récupération de toutes les catégories
    app.get("/api/V01/Categories/all", CategoryControllers.getAllCategories)

    //Route de récupération d'une catégorie
    app.get("/api/V01/Categories/:id", CategoryControllers.getOneCategory)

    //Route d'ajout d'une nouvelle catégorie
    app.post("/api/V01/Categories/save", adminAuth, CategoryControllers.saveOneCategory)

    //Route de modification d'une catégorie
    app.put("/api/V01/Categories/edit/:id", adminAuth, CategoryControllers.editOneCategory)

    //route de suppresion d'une catégorie
    app.delete("/api/V01/Categories/delete/:id", adminAuth, CategoryControllers.deleteOneCategory)
}