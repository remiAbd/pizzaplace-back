const withAuth = require('../withAuth');
const adminAuth = require('../adminAuth');
const employeeAuth = require('../employeeAuth');



//Code de gestion des commandes
module.exports = (app, db, configsecrets) => {
    const orderControllers = require("../controllers/orderControllers")(db)
    // console.log(configsecrets)

    //route d'enregistrement d'une commande
    app.post("/api/V01/order/save", orderControllers.saveOneOrder)

    //Route de modification de status d'une commande
    app.put("/api/V01/order/changeStatus/:id", employeeAuth, orderControllers.changeStatus)

    //Route de modification de status pour marquer l'envoi
    app.put("/api/V01/order/setShipped/:id", employeeAuth, orderControllers.setShipped)

    //route de suppression d'une commande
    app.delete("/api/V01/order/delete/:id", employeeAuth, orderControllers.deleteOneOrder)

    //Route de récupération de toutes les commandes
    app.get("/api/V01/orders/all", employeeAuth, orderControllers.getAllOrders)

    //Route de récupération de toutes les commandes d'un client
    app.get("/api/V01/orders/from/all/:id", withAuth, orderControllers.getAllOrdersFrom)

    //Route de gestion de paiement
    app.post("/api/V01/order/payment/", orderControllers.orderPayment)

    //Route de récupération d'une commande
    app.get("/api/V01/order/one/:id", orderControllers.getOneOrder)

    //route de récupération des détails d'une commande
    app.get("/api/V01/order/details/:id", orderControllers.getOrderDetails)

    //route de récupération des détails complets d'une commande
    app.get("/api/V01/order/full/:id", orderControllers.getFullDetails)

    //Route de récupération des commandes en cours
    app.get("/api/V01/orders/current/", employeeAuth, orderControllers.getAllCurrentOrders)

    //Route de récupération des anciennes commandes
    app.get('/api/V01/orders/past', employeeAuth, orderControllers.getAllPastOrders)
    
    app.put("/api/V01/orders/setPaid/:id", orderControllers.setPaid)
}