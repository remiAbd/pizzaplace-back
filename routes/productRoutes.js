const withAuth = require('../withAuth');
const adminAuth = require('../adminAuth');
const employeeAuth = require('../employeeAuth');
const fs = require('fs')

module.exports = (app, db, configSecret) => {
    const ProductControllers = require("../controllers/productControllers")(db)

    //Route de sauvegarde d'un produit
    app.post("/api/V01/products/save", adminAuth, ProductControllers.saveOneProduct)

    //route de sauvegarde d'une image
    app.post("/api/V01/image/save", adminAuth, ProductControllers.saveOnePicture)

    //route de suppression d'un produit
    app.delete("/api/V01/products/delete/:id", adminAuth, ProductControllers.deleteOneProduct)

    //route de modification d'un produit
    app.put("/api/V01/products/edit/:id", adminAuth, ProductControllers.editOneProduct)

    //Route de récupération de tous les produits
    app.get("/api/V01/products/all", ProductControllers.getAllProducts)

    //Route de récupération d'un produit
    app.get("/api/V01/products/one/:id", ProductControllers.getOneProduct)
}
