const withAuth = require('../withAuth');
const adminAuth = require('../adminAuth');
const employeeAuth = require('../employeeAuth');

module.exports = (app, db, configsecrets) => {
    const restaurantControllers = require("../controllers/restaurantControllers")(db)
    console.log(configsecrets)

    //route de récupération du restaurant
    app.get("/api/V01/restaurant/get", restaurantControllers.getOneRestaurant)

    //route de sauvegarde du restaurant
    app.post("/api/V01/restaurant/save", adminAuth, restaurantControllers.saveOneRestaurant)

    //route de modification d'un restaurant
    app.put("/api/V01/restaurant/edit/:id", adminAuth, restaurantControllers.editOneRestaurant)

    //route de suppression d'un restaurant
    app.delete("/api/V01/restaurant/delete/:id", adminAuth, restaurantControllers.deleteOneRestaurant)
}