const bcrypt = require('bcryptjs');
const saltRounds = 10;
const jwt = require('jsonwebtoken');

const withAuth = require('../withAuth');

//Code de vérification du token de l'utilisateur
module.exports = (app, db)=>{
    const AuthControllers = require('../controllers/authControllers')(db);
    
    //route de recup de vérification d'un utilisateur à reconnecter
    app.get('/api/v01/auth/checkToken', withAuth, AuthControllers.authentication)
}