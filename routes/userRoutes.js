const withAuth = require('../withAuth');
const adminAuth = require('../adminAuth');
const employeeAuth = require('../employeeAuth');




module.exports = (app, db, configSecret)=>{
	const userControllers = require('../controllers/userControllers')(db)
	// console.log(configSecret)

    //Route d'ajout d'un client
    app.post("/api/V01/customer/save", userControllers.saveOneCustomer)

    //route de modification d'un client
    app.put("/api/V01/customer/edit/:key_id", withAuth, userControllers.editOneCustomer)

    //Route de suppression d'un client
    app.delete("/api/V01/customer/delete/:key_id", withAuth, userControllers.deleteOneCustomer)

    //Route de récupération des clients
    app.get("/api/V01/customer/all", adminAuth, userControllers.getAllCustomers)

    //route de récupération des informations d'un client
    app.get("/api/V01/customer/:key_id", withAuth, userControllers.getCustomerByKeyId)

    //Route get d'affichage de la page de validation de compte
    app.get("/api/V01/customer/validate/:key_id", userControllers.displayValidate)

    //Route de validation du compte
    app.post("/api/V01/customer/validation/:key_id", userControllers.validate)

    //route de demande de récupération de mot de pass oublié
    app.post("/api/v01/customer/forgot/", userControllers.forgot)

    //route d'affichage du template de modification de password (ejs)
    app.get("/api/v01/customer/reset_password/:key_id", userControllers.displayChangePassword)

    //route de modification du mot de passe
    app.post("/api/v01/customer/changePassword/:key_id", userControllers.changePassword)

    //Route de récupération d'un client
    app.get("/api/V01/customer/byId/:id", userControllers.getOneCustomer)

    //Route d'ajout d'un employé
    app.post("/api/V01/employee/save", adminAuth, userControllers.saveOneEmployee)

    //Route de modification d'un employé
    app.put("/api/V01/employee/edit/:id", adminAuth, userControllers.editOneEmployee)

    //Route de suppression d'un employé
    app.delete("/api/V01/employee/delete/:id", adminAuth, userControllers.deleteOneEmployee)

    //Route de récupération des employés
    app.get("/api/V01/employee/all", adminAuth, userControllers.getAllEmployees)

    //Route de récupération d'un employé
    app.get("/api/V01/employee/one/:id", employeeAuth, userControllers.getOneEmployee)

    //Route de connexion d'un utilisateur
    app.post("/api/V01/user/login", userControllers.login)
}