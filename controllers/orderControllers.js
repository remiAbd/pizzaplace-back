const axios = require("axios");
const stripe = require('stripe')('sk_test_51NSJKxDAi9rs7GgEsk39rM0uMj74GwIStuIad3zcCg1KMCLYhN24RfPLZ9nfEAoAbhKKeF0MsvavrL5lb7uduBoP002kzEwsmE');
const OrderModel = require("../models/OrderModels");


module.exports = (app, db, configsecrets) => {
    return OrderControllers
}

class OrderControllers {
    //Controlleur de sauvegarde d'une commande
    static async saveOneOrder(req, res, next) {
        //On utilise l'api geocoding pour récupérer la position de l'adresse de livraison, si la livraison est demandée
        if (req.body.addressLineOne){
            let address = `${req.body.addressLineOne} ${req.body.postalCode} ${req.body.city}`
            const encodedAddress = encodeURIComponent(address)
            let config = {
                method : "get",
                url: `https://api.geoapify.com/v1/geocode/search?text=${encodedAddress}&format=json&apiKey=4b79997af8dc4d22ba4f4461237c8704`,
                headers: { }
            }
            //On veut récupérer les coordonnés de l'adresse 
            await axios(config)
            .then((res) => {
                req.body.latitude = res.data.results[0].lat
                req.body.longitude = res.data.results[0].lon
            })
            .catch(err => console.log(err))
        } else {
            //Si la livraison n'est pas demandée, on met null aux coordonnées et l'adresse sera constituée de chaines de caractères vides
            req.body.latitude = null
            req.body.longitude = null
        }
        //On enregistre la commande dans la base de données
        await OrderModel.saveOneOrder(req, async (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                //Si l'enregistrement a fonctionné, on enregistre le détail de la commande.
                //boucle sur les élements de la commandes pour les enregistrer un à un
                for (let i = 0; i < req.body.basket.length; i++) {
                    //On enregistre chaque produit de la commande dans la base de données
                    await OrderModel.saveOrderDetail(results.insertId, req.body.basket[i], (err2, results2) => {
                        //S'il y a une erreur
                        if (err2) {
                            //On envoie un json d'erreur au front
                            res.json({status : 500, msg : "Erreur serveur", err : err2})
                        }
                    })
                }
                //Ensuite on envoie un json au front avec l'id de la commande
                res.json({status : 200, msg : "commande enregistrée", orderId : results.insertId})
            }
        })
    }

    //Controlleur de modification de status d'une commande
    static async changeStatus(req, res, next) {
        //On vérifie que la commande que l'on veut modifier existe
        await OrderModel.getOneOrder(req.params.id, async (err, results) => {
            //Si il y a une erreur
            if (err) {
                //On envoie un json d'erreur
                res.json({status : 500, msg : "Erreur serveur", err : err})
            //Sinon, 
            } else {
                //Si on ne trouve pas la commande
                if(results.length === 0) {
                    //On envoie un json d'erreur au front
                    res.json({status : 400, msg : "commande introuvable"})
                //Sinon
                } else {
                    //On modifie le status de la commande
                    await OrderModel.updateOrderStatus(req.body.status, req.params.id, (err2, results2) => {
                        //S'il y a une erreur
                        if (err2) {
                            //On envoie un json d'erreur au front
                            res.json({status : 500, msg : "Erreur serveur", err : err2})
                        //Sinon,
                        } else {
                            //On envoie un json au front
                            res.json({status : 200, msg : "Status modifié"})
                        }
                    })
                }
            }
        })
    }

    //Controlleur de modification de status pour marquer l'envoi
    static async setShipped(req, res, next) {
        //On vérifie que la commande que l'on veut modifier existe
        await OrderModel.getOneOrder(req.params.id, async (err, results) => {
            //Si il y a une erreur
            if (err) {
                //On envoie un json d'erreur
                res.json({status : 500, msg : "Erreur serveur", err : err})
            //Sinon, 
            } else {
                //Si on ne trouve pas la commande
                if(results.length === 0) {
                    //On envoie un json d'erreur au front
                    res.json({status : 400, msg : "commande introuvable"})
                //Sinon
                } else {
                    //On modifie le status de la commande
                    await OrderModel.setShipped(req.params.id, (err2, results2) => {

                    })
                    //S'il y a une erreur
                    if (err2) {
                        //On envoie un json d'erreur au front
                        res.json({status : 500, msg : "Erreur serveur", err : err2})
                    //Sinon,
                    } else {
                        //On envoie un json au front
                        res.json({status : 200, msg : "Status modifié"})
                    }
                }
            }
        })
    }

    //Controlleur de suppression d'une commande
    static async deleteOneOrder(req, res, next) {
        //On vérifie bien que la commande existe
        await OrderModel.getOneOrder(req.params.id, async (err, results) => {
            //Si il y a une erreur
            if (err) {
                //On envoie un json d'erreur
                res.json({status : 500, msg : "Erreur serveur", err : err})
            //Sinon, 
            } else {
                //On supprime la commande
                await OrderModel.deleteOneOrder(req.params.id, (err2, results2) => {

                })
                //S'il y a une erreur
                if (err2) {
                    //On envoie un json d'erreur au front
                    res.json({status : 500, msg : "Erreur serveur", err : err2})
                //Sinon
                } else {
                    //On envoie un jsin au front
                    res.json({status : 200, msg : "Commande supprimée"})
                }
            }
        })
    }

    //Controlleur de récupération de toutes les commandes
    static async getAllOrders(req, res, next) {
        await OrderModel.getAllOrders((err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                if (results.length === 0) {
                    res.json({status : 400, msg : "Aucunes commandes trouvées"})
                } else {
                    res.json({status : 200, msg : "Commandes", results : results})
                }
            }
        })
    }

    //Controlleur de récupération de toutes les commandes d'un client
    static async getAllOrdersFrom(req, res, next) {
        await OrderModel.getAllOrdersFrom(req.params.id, (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                res.json({status : 200, results : results})
            }
        })
    }

    //Controlleur de paiement de commande
    static async orderPayment(req, res, next) {
        await OrderModel.getOneOrder(req.body.orderId, async (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err :err})
            } else {
                const paymentIntent = await stripe.paymentIntents.create({
                    amount : results[0].amount*100,
                    currency : "eur",
                    payment_method_types: ['card'],
                    // metadata : {integration_check : "accept_a_payment"}
                })
                res.json({client_secret : paymentIntent.client_secret})
            }
        })
    }

    //Controlleur de récupération d'une commande
    static async getOneOrder(req, res, next) {
        await OrderModel.getOneOrder(req.params.id, (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                res.json({status : 200, results : results[0]})
            }
        })
    }

    //Controlleur de récupération des détails d'une commande
    static async getOrderDetails(req, res, next) {
        await OrderModel.getOneOrder(req.params.id, async (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err :err})
            } else {
                await OrderModel.getOrderDetails(req.params.id, (err2, results2) => {
                    if (err2) {
                        res.json({status : 500, msg : "Erreur serveur", err : err2})
                    } else {
                        res.json({status : 200, results : results2})
                    }
                })
            }
        })
    }

    //Controlleur de récupération de tous les détails d'une commande
    static async getFullDetails(req, res, next) {
        await OrderModel.getFullDetails(req.params.id, (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                res.json({status : 200, results : results})
            }
        })
        
    }

    //Controlleur de récupération de toutes les commandes en cours
    static async getAllCurrentOrders(req, res, next) {
        await OrderModel.getAllCurrentOrders((err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                res.json({status : 200, results : results})
            }
        })
    }

    //Controlleur de récupération de toutes les anciennes commandes
    static async getAllPastOrders(req, res, next) {
        await OrderModel.getAllPastOrders((err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                res.json({status : 200, results : results})
            }
        })
    }

    //Controlleur de modification de status pour marquer le paiement
    static async setPaid(req, res, next) {
        console.log("ici")
        await OrderModel.getOneOrder(req.params.id, async (err, results) => {
            if (err) {
                console.log(err)
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                console.log("results1 : ", results)
                await OrderModel.setPaid(req.params.id, (err2, results2) => {
                    if (err2) {
                        console.log(err)
                        res.json({status : 500, msg : "Erreur serveur", err : err2})
                    } else {
                        console.log("results : ", results2)
                        res.json({status : 200, msg : "Status modifié"})
                    }
                })
            }
        })
    }
}