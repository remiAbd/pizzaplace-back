const fs = require('fs')
const ProductModel = require('../models/ProductModels');

module.exports = (app, db, configSecret) => {
    return ProductControllers
}

class ProductControllers {
    //Controlleur de sauvegarde d'un produit
    static async saveOneProduct(req, res, next) {
        await ProductModel.saveOneProduct(req, (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", error : err})
            } else {
                res.json({status : 200, msg : "Produit sauvegardé"})
            }
        })
    }

    //Controlleur de sauvegarde d'une image
    static async saveOnePicture(req, res, next) {
        if(!req.files || Object.keys(req.files).length === 0) {
            res.json({status : 400, msg : "Image non récupérée"})
        } else {
            req.files.image.mv(`public/images/${req.files.image.name}`, (err) => {
                if (err) {
                    res.json({status : 500, msg : "Erreur serveur, l'image n'a pas pu être sauvegardée"})
                }
            })
            res.json({status : 200, msg : "Image sauvegardée", url : req.files.image.name})
        }
    }

    //Controlleur de suppression d'un produit
    static async deleteOneProduct(req, res, next) {
        await ProductModel.getOneProduct(req.params.id, async (err, results) => {
            if(err) {
                res.json({status : 500, msg : "Erreur serveur", error : err})
            } else {
                if(results[0].url !== `no-pict.jpg`){
                    fs.unlink(`public/images/${product[0].url}`, (err) => {
                        if(err){
                            res.json({status : 500, msg : "Erreur lors de la suppression de l'image", error : err})   
                        }
                    })   
                }
                await ProductModel.deleteOneProduct(req.params.id, (err2, results2) => {
                    if(err2) {
                        res.json({status : 500, msg : "Erreur serveur", error : err2})
                    } else {
                        res.json({status : 200, msg : "Produit supprimé"})
                    }
                })
            }
        })

    }

    //Controlleur de modification d'un produit
    static async editOneProduct(req, res, next) {
        await ProductModel.getOneProduct(req.params.id, async (err, results) => {
            if(err) {
                res.json({status : 500, msg : "Erreur serveur", error : err})
            } else {
                await ProductModel.editOneProduct(req, req.params.id, (err2, results2) => {
                    if(err2) {
                        res.json({status : 500, msg : "Erreur serveur", error : err2})
                    } else {
                        res.json({status : 200, msg : "Produit modifié"})
                    }
                })
            }
        })
    }

    //Controlleur de récupération des produits
    static async getAllProducts(req, res, next) {
        await ProductModel.getAllProducts((err, results) => {
            //si les produits ne sont pas récupérés correctement, on envoie une erreur vers le front
            if(err) {
                res.json({status : 500, msg : "erreur serveur", error : err})
                //Sinon, on envoie les produits
            } else {
                res.json({status : 200, results : results})
            }
        })
    }

    //Controlleur de récupération d'un produit par son id
    static async getOneProduct(req, res, next) {
        await ProductModel.getOneProduct(req.params.id, (err, results) => {
            //si le produit n'est pas récupéré correctement, on envoie une erreur vers le front
            if(err) {
                res.json({status : 500, msg : "erreur serveur", error : err})
                //Sinon, on envoie le produit
            } else {
                res.json({status : 200, results : results[0]})
            }
        })
    }
}