const axios = require("axios")
const RestaurantModel = require("../models/RestaurantModels")

module.exports = (db) => {
    return RestaurantController
}

class RestaurantController {

    //Controlleur de sauvegarde d'un restaurant
    static async saveOneRestaurant(req, res, next) {
        let address = `${req.body.addressLineOne} ${req.body.postalCode} ${req.body.city}`
        const encodedAddress = encodeURIComponent(address)
        let config = {
            method : "get",
            url : `https://api.geoapify.com/v1/geocode/search?text=${encodedAddress}&format=json&apiKey=4b79997af8dc4d22ba4f4461237c8704`,
            headers :  { }
        }
        await axios(config)
        .then((res) => {
            req.body.latitude = res.data.results[0].lat
            req.body.longitude = res.data.results[0].lon
        })
        .catch(err => console.log(err))

        await RestaurantModel.saveRestaurant(req, (err, results) => {
            if(err) {
                res.json({status : 500, msg : "Erreur serveur", err : errorMonitor})
            } else {
                res.json({status : 200, msg : "Restaurant enregistré !"})
            }
        })
    }

    //Controlleur de récupération d'un restaurant
    static async getOneRestaurant(req, res, next) {
        await RestaurantModel.getRestaurant((err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                res.json({status : 200, results : results[0]})
            }
        })
    }

    //Controlleur de modification d'un restaurant
    static async editOneRestaurant(req, res, next) {
        let address = `${req.body.addressLineOne} ${req.body.postalCode} ${req.body.city}`
        const encodedAddress = encodeURIComponent(address)
        let config = {
            method : "get",
            url : `https://api.geoapify.com/v1/geocode/search?text=${encodedAddress}&format=json&apiKey=4b79997af8dc4d22ba4f4461237c8704`,
            headers :  { }
        }
        await axios(config)
        .then((res) => {
            req.body.latitude = res.data.results[0].lat
            req.body.longitude = res.data.results[0].lon
        })
        .catch(err => console.log(err))

        await RestaurantModel.updateRestaurant(req.params.id, req, (err, results) => {
            if(err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                res.json({status : 200, msg : "Restaurant modifié !"})
            }
        })
    }

    //Controlleur de suppression d'un restaurant
    static async deleteOneRestaurant(req, res, next) {
        await RestaurantModel.deleteRestaurant(req.params.id, (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", err : err})
            } else {
                res.json({status : 200, msg : "Restaurant supprimé"})
            }
        })
    }
}