const mail = require('../lib/mailing');
const utf8 = require("utf8")
const axios = require("axios")
const Math = require("math")
const schedule = require("node-schedule")
const bcrypt = require('bcryptjs');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const secret = "pizza"
const UserModel = require("../models/UserModels");

module.exports = (app,db, configSecret) => {
    return UserController
}


class UserController {
    
//Controlleur d'ajout d'un client 
static async saveOneCustomer(req, res, next) {
    await UserModel.getCustomerByMail(req.body.email, async (err, results) => {
        if (err){
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            if (results.length !== 0) {
                res.json({status: 400, msg : "Utilisateur déja existant pour cet email"})
            } else {
                let adresse = `${req.body.addressLineOne} ${req.body.postalCode} ${req.body.city}`
                const encodedString = encodeURIComponent(adresse);
                var config = {
                    method: 'get',
                    url: `https://api.geoapify.com/v1/geocode/search?text=${encodedString}&format=json&apiKey=4b79997af8dc4d22ba4f4461237c8704`,
                    headers: { }
                }; 
                await axios(config)
                .then(function (response) {
                    req.body.latitude = response.data.results[0].lat
                    req.body.longitude = response.data.results[0].lon
                })
                .catch((error) => {
                    console.log(error);
                });
                await UserModel.saveOneCustomer(req, (err2, results2) => {
                    if(err2) {
                        res.json({status : 500, msg : "Erreur serveur lors de l'enregistrement"})
                    } else {
                        mail(
                            req.body.email,
                            "Validation email",
                            "Bienvenue sur PizzaPlace",
                            `Pour valider votre mail, cliquez <a href="http://localhost:9000/api/v01/customer/validate/${results2.key_id}">ici</a>!`
                        )
                        res.json({status : 200, msg : "Utilisateur enregistré"})
                    }
                })
            }
        }
    })
    
}

//Controlleur de modification d'un client
static async editOneCustomer(req, res, next) {
    await UserModel.getCustomerByKeyId(req.params.key_id, async (err, results) => {
        if (err){
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            if (results.length === 0) {
                res.json({status: 400, msg : "L'utilisateur n'existe pas"})
            } else {
                let newEmail = false;
                if(results[0].email !== req.body.email) {
                    newEmail = true;
                }
                let adresse = `${req.body.addressLineOne} ${req.body.postalCode} ${req.body.city}`
                const encodedString = encodeURIComponent(adresse);
                var config = {
                    method: 'get',
                    url: `https://api.geoapify.com/v1/geocode/search?text=${encodedString}&format=json&apiKey=4b79997af8dc4d22ba4f4461237c8704`,
                    headers: { }
                }; 
                await axios(config)
                .then(function (response) {
                    req.body.latitude = response.data.results[0].lat
                    req.body.longitude = response.data.results[0].lon
                })
                .catch(function (error) {
                    console.log(error);
                });
    
                await UserModel.updateCustomer(req, req.params.key_id, newEmail, (err2, results2) => {
                    if(err2) {
                        res.json({status : 500, msg : "Erreur serveur lors de la modification"})
                    } else {
                        res.json({status : 200, msg : "Utilisateur modifié"})
                    }
                })
            }
        }
    })
}

//Controlleur de suppression d'un client
static async deleteOneCustomer(req, res, next) {
    await UserModel.getCustomerByKeyId(req.params.key_id, async (err, results) => {
        if (err){
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            if (results.length === 0) {
                res.json({status: 400, msg : "L'utilisateur n'existe pas"})
            } else {
                await UserModel.deleteCustomer(req.params.key_id, (err2, results2) => {
                    if(err2) {
                        res.json({status : 500, msg : "Erreur serveur lors de la suppression"})
                    } else {
                        res.json({status : 200, msg : "Utilisateur supprimé"})
                    }
                })
            }
        }
    })
}

//Controlleur de récupération des clients
static async getAllCustomers(req, res, next) {
    //On récupère tous les clients dans la base de données
    await UserModel.getAllCustomers((err, results) => {
        //Si il y a une erreur, on envoi un message d'erreur vers le front
        if (err) {
            res.json({status : 500, msg : "Erreur serveur"})
        //Si tout est bon, on vérifie qu'on récupère bien des clients
        } else {
            if (results.length === 0) {
                //Si la liste est vide, on envoi un message d'erreur vers le front
                res.json({status : 400, msg : "Aucuns clients trouvés"})
            } else {
                //Si on en récupère bien, alors on envoi les clients vers le front
                res.json({status : 200, results : results})
            }
        }
    })
}

//Controlleur de récupération des informations d'un client
static async getCustomerByKeyId(req, res, next) {
    await UserModel.getCustomerByKeyId(req.params.key_id, (err, results) => {
        if (err) {
            res.json({status : 500, msg : "Erreur serveur"})
        //Si tout est bon, on vérifie qu'on récupère bien des clients
        } else {
            if (results.length === 0) {
                //Si la liste est vide, on envoi un message d'erreur vers le front
                res.json({status : 400, msg : "Aucun client trouvé"})
            } else {
                //Si on en récupère bien, alors on envoi les clients vers le front
                res.json({status : 200, results : results[0]})
            }
        }
    })
}

//Controlleur d'affichage de la page de validation de compte 
static async displayValidate(req, res, next) {
    res.render("../views/validate", {key_id : req.params.key_id, error : null})
}

//Controlleur de validation de compte
static async validate(req, res, next) {
    await UserModel.getCustomerByKeyId(req.params.key_id,async (err, results) => {
        if (err) {
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            await UserModel.updateValidateCustomer(req.params.key_id, (err2, results2) => {
                if (err2) {
                    res.json({status : 500, msg : "Erreur serveur"})
                } else {
                    res.json({status : 200, msg : "Client validé"})
                }
            })
        }
    })
}

//Controlleur de demande de récupération de mot de passe oublié
static async forgot(req, res, next) {
    await UserModel.getCustomerByMail(req.body.email, async (err, results) => {
        if (err) {
            //On envoie un json d'erreur
            res.json({status : 500, msg : "erreur serveur"})
        } else {
            if (results.length === 0) {
                res.json({status : 404, msg : "utilisateur non trouvé"})
            } else {
                await UserModel.updateKeyIdCustomer(req.body.email, (err2, results2) => {
                    //S'il y a une erreur :
                    if (err2) {
                        //On envoie une json d'erreur
                        res.json({status : 500, msg : "erreur serveur"})
                    } else {
                        //On envoie un mail
                        mail(
                            req.body.email,
                            "Changement de mot de passe",
                            "Mot de pass oublié ?",
                            `Pour modifier votre mot de passe, cliquez <a href="http://localhost:9000/api/v01/customer/reset_password/${results.key_id}">ici</a>!`
                        )
                        //On envoie un json de succès
                        res.json({status : 200, msg : "Email envoyé"})
                    }
                })
            }
        }
    })
}

//Controlleur d'affichage de la page de changement de mot de passe
static async displayChangePassword(req, res, next) {
    //On affiche la page de modification de mot de passe
    res.render("../views/forgot", {key_id : req.params.key_id, error : null})
}

//Controlleur de modification du mot de passe
static async changePassword(req, res, next) {
    let error = null
    if (req.body.password1 !== req.body.password2) {
        error = "Les mots de passe ne correspondent pas"
    } else {
        console.log("ici correct")
        UserModel.updatePasswordCustomer(req.body.password1, req.params.key_id, (err, results) => {
            if (err) {
                console.log("la code")
                //On envoie une json d'erreur
                error = "le mot de passe n'a pas pu être modifié"
            } else {
                //On envoie un json de succès
                console.log("ici tout good")
                error = "le mot de passe a bien été modifié"
            }
        })
    }
    res.render("../views/forgot", { key_id: req.params.key_id, error });
}

//Controlleur de récupération d'un client par son id
static async getOneCustomer(req, res, next) {
    await UserModel.getCustomerById(req.params.id, (err, results) => {
        if (customer.code) {
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            res.json({status : 200, results: customer})
        }
    })
}

//Controlleur d'ajout d'un employé
static async saveOneEmployee(req, res, next) {
    //Pour attribuer un code unique à un employé
    let code = 0;
    let found = false;
    //On effectue la boucle tant que found est false
    do {
        //On affecte au code un nombre aléatoire entre 9999 et 0
        code = Math.floor(Math.random() * 9999)
        //On vérifie que ce code est unique
        await UserModel.getEmployeeByCode(code, (err, results) => {
            if(err) {
                res.json({status : 500, msg : "Erreur serveur"})
            } else {
                //S'il est unique
                if (results.length === 0) {
                    //On définie found sur true et on sort de la boucle
                    found = true;
                }
            }
        })
    } while (found)
    req.body.code = code
    await UserModel.getEmployeeByMail(req.body.email, async (err, results) => {
        if (err) {
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            if (results.length > 0) {
                res.json({status : 400, msg : "Email déja utilisé pour cet employé"})
            } else {
                req.body.latitude = null
                req.body.longitude = null
                await UserModel.saveOneEmployee(req, (err2, results2) => {
                    if(err2) {
                        res.json({status : 500, msg : "Erreur serveur lors de l'enregistrement"})
                    } else {
                        res.json({status : 200, msg : "Employé enregistré"})
                    }
                })
                
            }
        }
    })
}

//Controlleur de modification d'un employé
static async editOneEmployee(req, res, next) {
    await UserModel.getEmployeeById(req.params.id, async (err, results) => {
        if (err) {
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            if (results.length === 0) {
                res.json({status : 400, msg : "Employé introuvable"})
            } else {
                await UserModel.editEmployee(req, req.params.id, (err2, results2) => {
                    if(err2) {
                        res.json({status : 500, msg : "Erreur serveur lors de la modification"})
                    } else {
                        res.json({status : 200, msg : "Employé modifié"})
                    }
                })
            }
        }
    })
}

//Controlleur de suppression d'un employé
static async deleteOneEmployee(req, res, next) {
    await UserModel.getEmployeeById(req.params.id, async (err, results) => {
        if (err) {
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            if (results.length === 0) {
                res.json({status : 400, msg : "Employé introuvable"})
            } else {
                await UserModel.deleteEmployee(req.params.id, (err2, results2) => {
                    if(err2) {
                        res.json({status : 500, msg : "Erreur serveur lors de la suppression"})
                    } else {
                        res.json({status : 200, msg : "Employé supprimé"})
                    }
                })
            }
        }
    })
}

//Controlleur de récupération des employés
static async getAllEmployees(req, res, next) {
    //On récupère tous les employés dans la base de données
    await UserModel.getAllEmployees((err, results) => {
        //Si il y a une erreur, on envoi un message d'erreur vers le front
        if (err) {
            res.json({status : 500, msg : "Erreur serveur"})
        //Si tout est bon, on vérifie qu'on récupère bien des employés
        } else {
            if (results.length === 0) {
                //Si la liste est vide, on envoi un message d'erreur vers le front
                res.json({status : 400, msg : "Aucuns employés trouvés"})
            } else {
                //Si on en récupère bien, alors on envoi les employés vers le front
                res.json({status : 200, results : results})
            }
        }
    })
}

//Controlleur de récupération d'un employé
static async getOneEmployee(req, res, next) {
    await UserModel.getEmployeeById(req.params.id, (err, results) => {
        //Si il y a une erreur, on envoi un message d'erreur vers le front
        if (err) {
            res.json({status : 500, msg : "Erreur serveur", err : err})
        //Si tout est bon, on vérifie qu'on récupère bien un employé
        } else {
            if (results.length === 0) {
                //Si la liste est vide, on envoi un message d'erreur vers le front
                res.json({status : 400, msg : "Aucun employé trouvé", err : results})
            } else {
                //Si on le récupère bien, alors on envoi l'employé vers le front
                res.json({status : 200, results : results[0]})
            }
        }
    })
}

//Controlleur de connexion d'un utilisateur
static async login(req, res, next) {
    //On essaye dans un premier temps de trouver l'utilisateur dans les clients
    await UserModel.getCustomerByMail(req.body.email, async (err, results) => {
        //S'il y a un problème, on envoi un message d'erreur vers le front
        if (err) {
            res.json({status : 500, msg : "Erreur serveur"})
        } else {
            //Si la requête fonctionne, on vérifie qu'on trouve bien un client
            if (results.length > 0) {
                //Si on en trouve un, alors on vérifie qu'il a bien validé son mail
                if(results[0].validate === "yes") {
                    //Si oui, on compare les mots de passe
                    let same = await bcrypt.compare(req.body.password, results[0].password)
                    //Si les mots de passe concordent, alors on le connecte et on envoie un token vers le front
                    if (same) {
                        const payload = {email : results[0].email, key_id : results[0].key_id, role : "customer"}
                        const token = jwt.sign(payload, secret)
                        res.json({status : 200, msg : "Utilisateur connecté", results : results[0], token : token})
                    //Si les mots de passe sont différents, alors on envoi un message d'erreur vers le front
                    } else {
                        res.json({status : 401, msg : "Email ou mot de passe incorrect"})
                    }
                //Sinon,
                } else {
                    //On envoi un json d'erreur
                    res.json({status : 400, msg : "Votre compte n'est pas validé, vérifiez vos mails"})
                }
            //Si on ne trouve aucuns clients, alors on cherche chez les employés
            } else {
                //On essaye de récupérer l'utilisateur correspondant au mail
                await UserModel.getEmployeeByMail(req.body.email, async (err2, results2) => {
                    //S'il y a un problème, on envoi un message d'erreur vers le front
                    if (results2.code) {
                        res.json({status : 500, msg : "Erreur serveur"})
                    } else {
                        //Si la requête fonctionne, on vérifie qu'on trouve bien un employé
                        if (results2.length > 0) {
                            //Si on en trouve un, alors on compare les mots de passe
                            let same = await bcrypt.compare(req.body.password, results2[0].password)
                            //Si les mots de passe concordent, alors on le connecte et on envoie un token vers le front
                            if (same) {
                                const payload = {email : results2[0].email, key_id : results2[0].key_id, role : results2[0].role}
                                const token = jwt.sign(payload, secret)
                                res.json({status : 200, msg : "Utilisateur connecté", results : results2[0], token : token})
                            } else {
                                //Si les mots de passe sont différents, alors on envoi un message d'erreur vers le front
                                res.json({status : 401, msg : "Email ou Mot de passe incorrect"})
                            }
                        //Si aucun utilisateur n'est trouvé, alors on envoi un message d'erreur vers le front
                        } else {
                            res.json({status : 400, msg : "Aucun utilisateur pour cet email, veuillez réessayer"})
                        }
                    }
                })
            }
        }
    })
    
}
}