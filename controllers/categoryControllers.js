const CategoryModels = require("../models/CategoryModels");

module.exports = (app, db, configSecret) => {
    return categoryController
}

class categoryController {
    //Controlleur de récupération de toutes les catégories
    static async getAllCategories(req, res, next) {
        await CategoryModels.getAllCategories((err, results) => {
            //si les categories ne sont pas récupérées correctement, on envoie une erreur vers le front
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", error : err})
            } else {
                //S'il n'y a pas de categories, on envoi une erreur vers le front
                if (results.length === 0) {
                    res.json({status : 400, msg : "Aucunes catégories trouvés"})
                } else {
                    //Si tout est bon, on envoi les categories vers le front
                    res.json({status : 200, results : results})
                }
            }
        })
    }

    //Controlleur de récupération d'une catégorie par son id
    static async getOneCategory(req, res, next) {
        await CategoryModels.getOneCategory(req.params.id, (err, results) => {
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", error :err})
            } else {
                res.json({status : 200, results : results[0]})
            }
        })
        
    }

    //Controlleur de sauvegarde d'une catégorie
    static async saveOneCategory(req, res, next) {
        //On sauvegarde la nouvelle catégorie dans la base de données
        await CategoryModels.saveOneCategory(req, (err, results) => {
            //S'il y a une problème lors de la sauvegarde, on envoi une erreur vers le front
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", error : err})
            } else {
                //Si tout est bon, on envoi un message de réussite
                res.json({status : 200, msg : "Catégorie crée"})
            }
        })
    }

    //Controlleur de modification d'une catégorie
    static async editOneCategory(req, res, next) {
        //On vérifie qu'on récupère bien la catégorie que l'on veut modifier
        await CategoryModels.getOneCategory(req.params.id, async (err, results) => {
            //S'il y a une problème lors de la récupération, on envoi une erreur vers le front
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", error : err})
            } else {
                //Si tout est bon, on modifie la catégorie
                await CategoryModels.editOneCategory(req, req.params.id, (err2, results2) => {
                    //S'il y a une problème lors de la modification, on envoi une erreur vers le front
                    if (err2) {
                        res.json({status : 500, msg : "Erreur serveur", error : err2})
                    } else {
                        //Si tout est bon, on envoi un message de réussite
                        res.json({status : 200, msg : "Catégorie modifiée"})
                    }
                })
            }
        })
    }

    //Controlleur de suppression d'une catégorie
    static async deleteOneCategory(req, res, next) {
        //On vérifie qu'on récupère bien la catégorie que l'on veut supprimer
        await CategoryModels.getOneCategory(req.params.id, async (err, results) => {
            //S'il y a une problème lors de la récupération, on envoi une erreur vers le front
            if (err) {
                res.json({status : 500, msg : "Erreur serveur", error : err})
            } else {
                //Si tout est bon, on supprime la catégorie
                await CategoryModels.deleteOneCategory(req.params.id, (err2, results2) => {
                    //S'il y a une problème lors de la suppression, on envoi une erreur vers le front
                    if (err2) {
                        res.json({status : 500, msg : "Erreur serveur", error : err2})
                    } else {
                        //Si tout est bon, on envoi un message de réussite
                        res.json({status : 200, msg : "Catégorie supprimée"})
                    }
                })
                
            }
        })
    }
}