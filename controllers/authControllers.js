const bcrypt = require('bcryptjs');
const saltRounds = 10;
const jwt = require('jsonwebtoken');

const withAuth = require('../withAuth');

const userModel = require("../models/UserModels");

module.exports = (app, db, configSecret) => {
    return AuthControllers
}

class AuthControllers {
    static async authentication(req, res, next) {
        //On récupère le client par son email
        await userModel.getCustomerByMail(req.email, async (err, results) => {
            //Si il y a une erreur
            if(err){
                //On envoi un json d'erreur
                res.json({status: 500, err: err})
            //Sinon,
            } else {
                //Si on n'a récupéré personne, alors c'est surement un employé
                if (results.length === 0) {
                    //On récupère l'employé par son email
                    await userModel.getEmployeeByMail(req.email, (err2, results2) => {
                        //Si il y a une erreur
                        if (err2) {
                            //On envoi un json d'erreur
                            res.json({status: 500, err: err2})
                        //Sinon,
                        } else {
                            //On envoi les informations de l'employé vers le front
                            res.json({status : 200, msg : "token valide", user :results2[0]})
                        }
                    })
                //Sinon,
                } else {
                    //On construit l'objet à envoyer vers le front
                    results[0].role = "customer"
                    let myUser = {
                        id: results[0].customersID,
                        email: results[0].email,
                        firstName: results[0].firstName,
                        lastName: results[0].lastName,
                        role: results[0].role,
                        url: results[0].url,
                        key_id: results[0].key_id
                    }
                    //On envoie les informations du client vers le front
                    res.json({status: 200, msg: "token valide", user: myUser})
                }
            }
        })
    }
}