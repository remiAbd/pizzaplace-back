const express = require("express")
const app = express()
const mysql = require("promise-mysql")
const fs = require("fs")
const utf8 = require("utf8")
const axios = require("axios")
const schedule = require('node-schedule')
app.set('views', './views');
app.set('view engine', 'ejs');

const fileUpload = require("express-fileupload")

app.use(fileUpload({
    createParentPath: true
}))

app.use(express.static(__dirname + "/public"))
app.use(express.urlencoded({extended : false}))
app.use(express.json())

const cors = require("cors")
app.use(cors())

let config
if(!process.env.HOST_DB) {
    config = require("./config-offline")
} else {
    config = require("./config")
}

const restaurantRoutes = require("./routes/restaurantRoutes")
const userRoutes = require("./routes/userRoutes")
const authRoutes = require("./routes/authRoutes")
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")
const categoryRoutes = require("./routes/categoryRoutes")

// const host = process.env.HOST_DB || config.db.host
// const database = process.env.DATABASE_DB || config.db.database
// const user = process.env.USER_DB || config.db.user
// const password = process.env.PASSWORD_DB || config.db.password
const secret = config.token.secret

// mysql.createConnection({
//     host : host,
//     database : database,
//     user : user,
//     password : password
// })
// .then((db) => {
//     console.log("connecté à la DB")
//     setInterval(async function () {
// 		let res = await db.query('SELECT 1');
// 	}, 10000);
	
// 	app.get('/', (req, res, next)=>{
// 	    res.json({status: 200, msg: "Welcome to api pizza"})
// 	})
const db = require("./db")
	//appel de nos routes
userRoutes(app,db, secret)
authRoutes(app,db, secret)
productRoutes(app,db, secret)
orderRoutes(app,db, secret)
restaurantRoutes(app, db, secret)
categoryRoutes(app, db, secret)
// })
// .catch(err=>console.log(err))

const PORT = process.env.PORT || 9000

app.listen(PORT, () => {
    console.log(`Listening port ${PORT} all is ok`)
})