const mysql = require('mysql');
let config
if(!process.env.HOST_DB) {
    config = require("./config-offline")
} else {
    config = require("./config")
}

const host = process.env.HOST_DB || config.db.host
const database = process.env.DATABASE_DB || config.db.database
const user = process.env.USER_DB || config.db.user
const password = process.env.PASSWORD_DB || config.db.password
const secret = config.token.secret

const db = mysql.createPool({
    connectionLimit: 10, // nombre maximum de connexions dans le pool
    host : host,
    database : database,
    user : user,
    password : password
});

// Vous n'avez pas besoin de la fonction db.connect() avec un pool

module.exports = db;


/*
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'react_crud1'
});

db.connect((err) => {
  if (err) {
    console.error('Erreur de connexion à la base de données:', err);
  } else {
    console.log('Connecté à MySQL!');
  }
});

module.exports = db;
*/